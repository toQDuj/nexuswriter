# NeXusWriter: a helper function for writing HDF5 files according to the NeXus standard. 

# Author: Brian R. Pauw
# License: GPLv3

This class combines a range of solutions developed over the years to write
HDF5 files in Python, and in particular for writing them into a NeXus-compliant
structure (an archival data storage format for X-ray and Neutron methods, 
see http://nexusformat.org/ ).

NeXusWriter allows both a direct input of a dictionary of paths to datasets
or attributes and their values, as well as an adjustable mapping of input
keyword-value pairs to NeXus/HDF5 locations.

Example usage:

    directDict = {'/sasentry1/simulationParameters/testgroup': 'Helix',
    '/sasentry1/simulationParameters/filename': 'models/Helix3.stl',
    '/sasentry1/simulationParameters/filename@operatingSystem': 'darwin',
    '/sasentry1/simulationParameters/projectDirectory': 'HelixTest/'}

    NeXusWriter(
        filename = 'something.nxs',
        Q = data['Q'], 
        I = data['I'],
        IError = data['IError'], 
        wavelength = 0.1542,
        wavelengthUnits = 'nm',
        title = 'Simulated data for testgroup: {}, file: {}'.format(
            rd['testgroup'], rd['filename']            
        ),
        timestamp = datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc).replace(microsecond=0).isoformat(),
        overwrite = True,
        directDict = directDict, 
	)

